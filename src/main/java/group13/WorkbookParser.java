package group13;


import org.apache.poi.ss.usermodel.*;

class WorkbookParser {

    protected Workbook workbook = null;
    protected DataFormatter dataFormatter = new DataFormatter();

    WorkbookParser(Workbook workbook) {
        this.workbook = workbook;
    }

    Cell getCell(Sheet sheet, int row, int col) {
        return sheet.getRow(row).getCell(col);
    }

    RowParser getRowParser(Row row) {
        return new RowParser(row);
    }

    class RowParser {
        protected Row row = null;

        protected RowParser(Row row) {
            this.row = row;
        }

        String getStringValue(int col) {
            return row.getCell(col).getStringCellValue();
        }

        int getIntValue(int col) {
            return Integer.parseInt(dataFormatter.formatCellValue(row.getCell(col)));
        }
    }
}
