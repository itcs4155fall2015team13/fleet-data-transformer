package group13;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.IOException;
import java.sql.*;

public class ParseWorkbook {

    final static String dbName = "fleet_data.db";
    final static File dataFile = new File("UNCC Meter Report.xlsx");

    static void createCarsTable(Connection conn) throws SQLException, IOException, InvalidFormatException {
        Statement statement = conn.createStatement();
        statement.setQueryTimeout(30);

        statement.executeUpdate("DROP TABLE IF EXISTS Cars");
        StringBuilder sb = new StringBuilder("CREATE TABLE Cars (");
        sb.append("Year INT").append(",");
        sb.append("Make TEXT").append(",");
        sb.append("Model TEXT").append(",");
        sb.append("MeterType TEXT").append(",");
        sb.append("Beginning INT").append(",");
        sb.append("Ending INT").append(",");
        sb.append("Usage INT").append(",");
        sb.append("Class TEXT").append(",");
        sb.append("DateRange TEXT").append(",");
        sb.append("SheetName TEXT");
        sb.append(")");
        statement.executeUpdate(sb.toString());

        // Open excel workbook
        Workbook wb = new XSSFWorkbook(dataFile);
        WorkbookParser wbp = new WorkbookParser(wb);
        DataFormatter dataFormatter = new DataFormatter();

        int nSheets = wb.getNumberOfSheets();

        for (int sheetI = 0; sheetI < nSheets; sheetI++) {
            Sheet sheet = wb.getSheetAt(sheetI);
            String sheetName = sheet.getSheetName();
            System.out.println("Parsing sheet '" + sheetName + "'");

            if (sheet.getRow(0) == null) {
                System.out.println("Sheet '" + sheetName + "' is empty; ignoring it");
            } else {
                String dateRange = sheet.getRow(1).getCell(6).getStringCellValue();
                String class_ = sheet.getRow(5).getCell(0).getStringCellValue();

                int rowI = 7;
                Row row;
                while ((row = sheet.getRow(rowI)).getCell(0).getCellType() == Cell.CELL_TYPE_BLANK) {
                    WorkbookParser.RowParser rowParser = wbp.getRowParser(row);
                    int year = rowParser.getIntValue(2);
                    String make = rowParser.getStringValue(4);
                    String model = rowParser.getStringValue(8);
                    String meterType = rowParser.getStringValue(10);
                    int beginning = rowParser.getIntValue(13);
                    int ending = rowParser.getIntValue(15);
                    int usage = rowParser.getIntValue(16);

                    StatementBuilder stb = new StatementBuilder("INSERT INTO Cars VALUES (");
                    stb.appendValue(year);
                    stb.appendValue(make);
                    stb.appendValue(model);
                    stb.appendValue(meterType);
                    stb.appendValue(beginning);
                    stb.appendValue(ending);
                    stb.appendValue(usage);
                    stb.appendValue(class_);
                    stb.appendValue(dateRange);
                    stb.appendValue(sheetName);
                    stb.append(")");

                    statement.executeUpdate(stb.toString());

                    rowI++;
                }
            }
        }
    }

    static void printCarsTable(Connection conn) throws SQLException {
        Statement statement = conn.createStatement();
        statement.setQueryTimeout(30);

        ResultSet rs = statement.executeQuery("SELECT * FROM Cars");
        while (rs.next()) {
            System.out.printf("%s\t%s\t%s%n",
                    rs.getInt("Year"), rs.getString("Make"), rs.getInt("Model"));
        }
    }

    public static void main(String[] args) {
        Connection conn = null;
        try {
            // Connect to database
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);

            createCarsTable(conn);

            printCarsTable(conn);

        } catch (IOException | InvalidFormatException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}