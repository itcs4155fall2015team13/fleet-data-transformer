package group13;

class StatementBuilder implements Appendable {
    protected StringBuilder sb = null;
    protected boolean lastAppendedValue = false;

    StatementBuilder() {
        this.sb = new StringBuilder();
    }

    StatementBuilder(String str) {
        this.sb = new StringBuilder(str);
    }

    @Override
    public StatementBuilder append(char c) {
        sb.append(c);
        lastAppendedValue = false;
        return this;
    }

    @Override
    public StatementBuilder append(CharSequence csq) {
        sb.append(csq);
        lastAppendedValue = false;
        return this;
    }

    @Override
    public StatementBuilder append(CharSequence csq, int start, int end) {
        sb.append(csq, start, end);
        lastAppendedValue = false;
        return this;
    }

    public StatementBuilder append(String str) {
        sb.append(str);
        lastAppendedValue = false;
        return this;
    }

    /**
     * Automatically inserts commas between calls to `appendValue`
     *
     *
     */
    public StatementBuilder appendValue(int integer) {
        if (lastAppendedValue) sb.append(", ");
        sb.append(String.format("%d", integer));
        lastAppendedValue = true;
        return this;
    }

    /**
     * Automatically inserts commas between calls to `appendValue`
     *
     * `text` is wrapped in single quotes before appending
     */
    public StatementBuilder appendValue(String text) {
        if (lastAppendedValue) sb.append(", ");
        sb.append(String.format("'%s'", text));
        lastAppendedValue = true;
        return this;
    }

    @Override
    public String toString() {
        return sb.toString();
    }
}
